import "./css/main.css";
console.log("object");
export const getDataFromForm = () => {
  console.log("getDataFromForm");
  document.querySelector("#main-form").addEventListener("submit", e => {
    console.log("submit");
    collectData(e, writeToFrame);
  });
};

getDataFromForm();

console.log("2");
console.log("2");

const writeToFrame = data => {
  console.log("wrtite to frame", data);
  const frame = document.querySelector(".blank__container").contentWindow;
  const app = frame.document.querySelector("#app");
  //   console.log(data, frame);
  const blank = data => {
    return `
            <h1 class="blank__title">Ваша воля спасти ${data.name}, ${
      data.age
    } лет</h1>
        <div class="blank__img-container">
            <img src="" alt="img">
        </div>
        <div class="blank__info">
            <div class="blank__info--right">

                <div class="blank__section">
                    <div class="section__title">
                        Диагноз
                    </div>
                    <div class="section__info section__info--size-l">
                        ${data.diagnosis}
                    </div>
                </div>

                <div class="blank__section ">
                    <div class="section__title">
                        Лечение
                    </div>
                    <div class="section__info section__info--size-m">
                        ${data.treatment}
                    </div>
                </div>

                <div class="blank__section ">
                    <div class="section__title">
                        Сумма
                    </div>
                    <div class="section__info section__info--size-l">
                        ${data.money}
                    </div>
                </div>

                <div class="blank__section ">
                    <div class="section__title">
                        Родственники
                    </div>
                    <div class="section__info section__info--size-m info">
                        <div class="info__title">${data.relative_name_1} ${
      data.relative_surname_1
    }</div>
                        <div class="info__desc">${data.relative_tel_1}</div>
                    </div>
                    <div class="section__info section__info--size-m info">
                        <div class="info__title">${data.relative_name_2} ${
      data.relative_surname_2
    }</div>
                        <div class="info__desc">${data.relative_tel_2}</div>
                    </div>

                </div>

            </div>
        </div>
  `;
  };
  app.innerHTML = blank(data);
  frame.print();
};

const collectData = (e, writeToFrame) => {
  console.log("Collect data");
  let data = {};
  e.preventDefault();
  Array.from(e.target).forEach(node => {
    if (
      node.nodeName === "INPUT" ||
      node.nodeName === "SELECT" ||
      node.nodeName === "TEXTAREA"
    ) {
      const temp = {
        [node.name]: node.value
      };
      Object.assign(data, temp);
    }
    // console.log(data);
  });
  writeToFrame(data);
  return data;
};
